# Google Maps Drop Down

Google maps use with drop down accordion.

## Version
0.0.1-alpha.1

## Development

Want to contribute? Great! contact us.

## Authors

 - Anderson Lopez M
 - John Jairo Tangarife V

## Todos

 - Add preview pictures
 - Add example bower
 - Check browsers compatibility
 - Check javascript good practices
 - Document how to config the project
 - Add suport for address
 - Add suport for initial country
 - Create style without specific theme

License
----

MIT


**Free Software, Hell Yeah!**