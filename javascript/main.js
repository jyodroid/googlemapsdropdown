function init() {
// Parse json
    var jsonObj = JSON.parse(this.responseText);
    var cities = jsonObj.cities;
    var styleMap = jsonObj.styleMap;

    var storesArray = createDropDown(cities);
    initialize(storesArray, styleMap);
    //google.maps.event.addDomListener(window, 'load', initMap(storesArray, styleMap));
}

//function initMap(stores, styleMap) {
//    return function () {
//        initialize(stores, styleMap);
//    };
//}

function createDropDown(cities) {
    var skeletonContainer =
        '<li class="accordion__item js-contentToggle">' +
        '<button class="accordion__trigger js-contentToggle__trigger" type="button">{{ cityName }}</button>' +
        '</li>';
    var skeletonData = '<div class="cities"><a id="store_{{id}}">{{ content }}</a></div>';

    var citiesContainer = '<div class="accordion__content is-hidden js-contentToggle__content city_scrollable"></div>';

    // Create accordion html
    var tagId = 0;
    var storesArray = [];
    for (var i = 0; i < cities.length; ++i) {
        var city = cities[i];
        var cityName = city.name;

        var stores = city.stores;

        var divHtml = $.parseHTML(citiesContainer);
        for (var j = 0; j < stores.length; ++j) {
            var store = stores[j];
            var storeName = store.name;
            var htmlData = Mustache.to_html(skeletonData, {content: storeName, id: tagId});
            tagId++;
            storesArray.push({store: store});
            $(divHtml).append(htmlData);
        }

        var htmlContainer = Mustache.to_html(skeletonContainer, {cityName: cityName});
        var htmlContainerNew = $.parseHTML(htmlContainer);
        $(htmlContainerNew).append(divHtml);
        $('#mapAccordion').append(htmlContainerNew);
    }

    // init accordion
    $(".js-contentToggle").contentToggle({
        toggleOptions: {
            duration: 400
        }
    }).trigger('close');

    return storesArray;
}

<!--Google Maps-->
function initialize(stores, styleMap) {
    var map = null;
    var zoomOriginal = 5;
    var zoomStores = 17;
    var colombiaCenter = new google.maps.LatLng(4.159755985156323, -72.93168350000002);
    var iconsPath = 'resources/icons/';
    var icons = {
        smowie: {
            name: 'Smowie',
            icon: iconsPath + 'Smowie.png'
        }
    };
    var mapOptions = {
        zoom: zoomOriginal,
        center: colombiaCenter,
        mapTypeControl: false,
        styles: styleMap
    };

    map = new google.maps.Map(document.getElementById('map'), mapOptions);

    // Add markers
    function addMarker(feature) {
        var imageBaseSrc = 'resources/images/';
        var infowindow = new google.maps.InfoWindow({
            content: '<h3><span class="store_tittle">' + feature.locationName + '</span></h3>' +
            '<h4 class="store_address">' + feature.address + '</h4>' +
            '<img class="store_image" src="' + imageBaseSrc + feature.source + '" width = "150"/>'
        });
        var marker = new google.maps.Marker({
            position: feature.position,
            icon: icons[feature.type].icon,
            map: map,
            animation: google.maps.Animation.DROP
        });
        google.maps.event.addListener(marker, 'click', function () {
            infowindow.open(map, marker);
        });
    }

    var features = [];
    var storesCenter = [];
    for (var i = 0; i < stores.length; i++) {
        var store = stores[i].store;
        var storeCenter = new google.maps.LatLng(store.x, store.y);
        var feature =
        {
            position: storeCenter,
            type: 'smowie',
            locationName: store.name,
            source: store.source,
            address: store.address
        }
        features.push(feature);
        storesCenter.push(storeCenter);
    }

    for (var i = 0, feature; feature = features[i]; i++) {
        addMarker(feature);
    }

    // Add center events
    function addNewCenterEvent(obj, center, zoom) {
        obj.addEventListener('click', function () {
            map.setZoom(zoom);
            map.setCenter(center);
        });
    }

    for (var i = 0; i < storesCenter.length; i++) {
        var storeId = 'store_' + i;
        var storeButton = document.getElementById(storeId);
        addNewCenterEvent(storeButton, storesCenter[i], zoomStores);
    }

    //responsive map and recenter
    google.maps.event.addDomListener(window, "resize", function () {
        var center = map.getCenter();
        google.maps.event.trigger(map, "resize");
        map.setCenter(center);
    });
}

// on load
(function () {
    var oReq = new XMLHttpRequest();
    oReq.open("get", "resources/config_map.json", true);
    oReq.onload = init;
    oReq.send();
})();