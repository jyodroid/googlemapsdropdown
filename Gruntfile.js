/**
 * Created by JohnTangarife on 8/02/16.
 */
module.exports = function (grunt) {
    grunt.initConfig({
        uglify: {
            target: {
                files: {
                    'dist/dropdownmap.min.js': [
                        'bower_components/**/dist/*.min.js',
                        'bower_components/**/jquery.contenttoggle.js',
                        'bower_components/mustache.js/mustache.js',
                        'javascript/*.js']
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-contrib-uglify');//Loads the given tasks
    grunt.registerTask('default', ['uglify']);//default task map to grunt

}